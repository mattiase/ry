;;; ry.el --- S-exp notation for regexps           --*- lexical-binding: t -*-

;; Author: Mattias Engdegård <mattiase@acm.org>
;; URL: https://gitlab.com/mattiase/ry
;; Keywords: strings, regexps

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This facility allows writing regexps in a sexp-based language
;; instead of strings.  Regexps in the `ry' notation are easier to
;; read, write and maintain; they can be indented and commented in a
;; natural way, and are easily composed by program code.
;; The translation to string regexp is done by a macro and does not
;; incur any extra processing during run time.  Example:
;;
;;  (rx bos (or (not (any "^"))
;;              (seq "^" (or " *" "["))))
;;
;; => "\\`\\(?:[^^]\\|\\^\\(?: \\*\\|\\[\\)\\)"
;;
;; The notation is much influenced by and retains some compatibility with
;; Olin Shivers's SRE, with concessions to Emacs regexp peculiarities,
;; and the older Emacs package Sregex.

;;; Code:

;; The `ry--translate...' functions below return (REGEXP . PRECEDENCE),
;; where REGEXP is a list of string expressions that will be
;; concatenated into a regexp, and PRECEDENCE is one of
;;
;;  t    -- can be used as argument to postfix operators
;;  seq  -- can be concatenated in sequence with other seq or higher
;;  lseq -- can be concatenated to the left of rseq or higher
;;  rseq -- can be concatenated to the right of lseq or higher
;;  nil  -- can only be used in alternatives
;; 
;; They form a lattice:
;;
;;           t          highest precedence
;;           |
;;          seq
;;         /   \
;;      lseq   rseq
;;         \   /
;;          nil         lowest precedence


(defconst ry--char-classes
  '((digit         . digit)
    (numeric       . digit)
    (num           . digit)
    (control       . cntrl)
    (cntrl         . cntrl)
    (hex-digit     . xdigit)
    (hex           . xdigit)
    (xdigit        . xdigit)
    (blank         . blank)
    (graphic       . graph)
    (graph         . graph)
    (printing      . print)
    (print         . print)
    (alphanumeric  . alnum)
    (alnum         . alnum)
    (letter        . alpha)
    (alphabetic    . alpha)
    (alpha         . alpha)
    (ascii         . ascii)
    (nonascii      . nonascii)
    (lower         . lower)
    (lower-case    . lower)
    (punctuation   . punct)
    (punct         . punct)
    (space         . space)
    (whitespace    . space)
    (white         . space)
    (upper         . upper)
    (upper-case    . upper)
    (word          . word)
    (wordchar      . word)
    (unibyte       . unibyte)
    (multibyte     . multibyte))
  "Alist mapping ry symbols to character classes.
Most of the names are from SRE.")

;; FIXME: Give initial value nil when replacing the old rx.el.
(defvar rx-constituents)

(defvar ry--local-definitions nil
  "Alist of local ry definitions.  Takes precedence to ry--global-definitions.
Each entry is:
 (NAME DEF)      -- NAME is an ry symbol defined as DEF.
 (NAME ARGS DEF) -- NAME is an ry form with arglist ARGS
                    to be substituted into DEF.")

(defvar ry--global-definitions nil
  "Alist of global ry definitions.
Each entry is:
 (NAME DEF)      -- NAME is an ry symbol defined as DEF.
 (NAME ARGS DEF) -- NAME is an ry form with arglist ARGS
                    to be substituted into DEF.")

(defsubst ry--lookup-def (name)
  (or (assq name ry--local-definitions)
      (assq name ry--global-definitions)))

;; TODO: Additions to consider:
;; - A better name for `anything', like `any-char' or `anychar'.
;; - A name for (or), maybe `unmatchable'.
;; - A construct like `or' but without the match order guarantee,
;;   maybe `unordered-or'.  Useful for composition or generation of
;;   alternatives; permits more effective use of regexp-opt.

(defun ry--translate-symbol (sym)
  "Translate an ry symbol.  Return (REGEXP . PRECEDENCE)."
  (pcase sym
    ((or 'nonl 'not-newline 'any) (cons (list ".") t))
    ('anything                    (ry--translate-form '(or nonl "\n")))
    ((or 'bol 'line-start)        (cons (list "^") 'lseq))
    ((or 'eol 'line-end)          (cons (list "$") 'rseq))
    ((or 'bos 'string-start 'bot 'buffer-start) (cons (list "\\`") t))
    ((or 'eos 'string-end   'eot 'buffer-end)   (cons (list "\\'") t))
    ('point                       (cons (list "\\=") t))
    ((or 'bow 'word-start)        (cons (list "\\<") t))
    ((or 'eow 'word-end)          (cons (list "\\>") t))
    ('word-boundary               (cons (list "\\b") t))
    ('not-word-boundary           (cons (list "\\B") t))
    ('symbol-start                (cons (list "\\_<") t))
    ('symbol-end                  (cons (list "\\_>") t))
    ('not-wordchar                (cons (list "\\W") t))
    (_ 
     (cond
      ((let ((class (cdr (assq sym ry--char-classes))))
         (and class (cons (list (concat "[[:" (symbol-name class) ":]]")) t))))

      ((let ((definition (ry--lookup-def sym)))
         (and definition
              (if (cddr definition)
                  (error "Not an `ry' symbol definition: %s" sym)
                (ry--translate (nth 1 definition))))))

      ;; For compatibility with old rx.
      ((let ((entry (assq sym rx-constituents)))
         (and (progn
                (while (and entry (not (stringp (cdr entry))))
                  (setq entry
                        (if (symbolp (cdr entry))
                            ;; Alias for another entry.
                            (assq (cdr entry) rx-constituents)
                          ;; Wrong type, try further down the list.
                          (assq (car entry)
                                (cdr (memq entry rx-constituents))))))
                entry)
              (cons (list (cdr entry)) nil))))
      (t (error "Unknown ry symbol `%s'" sym))))))
    
(defun ry--enclose (left-str rexp right-str)
  "Bracket REXP by LEFT-STR and RIGHT-STR."
  (append (list left-str) rexp (list right-str)))

(defun ry--bracket (rexp)
  (ry--enclose "\\(?:" rexp "\\)"))

(defun ry--sequence (left right)
  "Return the sequence (concatenation) of two translated items,
each on the form (REGEXP . PRECEDENCE), returning (REGEXP . PRECEDENCE)."
  ;; Concatenation rules:
  ;;  seq  ++ seq  -> seq
  ;;  lseq ++ seq  -> lseq
  ;;  seq  ++ rseq -> rseq
  ;;  lseq ++ rseq -> nil
  (cond ((not (car left)) right)
        ((not (car right)) left)
        (t
         (let ((l (if (memq (cdr left) '(nil rseq))
                      (cons (ry--bracket (car left)) t)
                    left))
               (r (if (memq (cdr right) '(nil lseq))
                      (cons (ry--bracket (car right)) t)
                    right)))
           (cons (append (car l) (car r))
                 (if (eq (cdr l) 'lseq)
                     (if (eq (cdr r) 'rseq)
                         nil                   ; lseq ++ rseq
                       'lseq)                  ; lseq ++ seq
                   (if (eq (cdr r) 'rseq)
                       'rseq                   ; seq ++ rseq
                     'seq)))))))               ; seq ++ seq

(defun ry--translate-seq (body)
  "Translate a sequence of one or more ry items.  Return (REGEXP . PRECEDENCE)."
  (if body
      (let* ((items (mapcar #'ry--translate body))
             (result (car items)))
        (dolist (item (cdr items))
          (setq result (ry--sequence result item)))
        result)
    (cons nil 'seq)))

(defun ry--empty ()
  "Regexp that never matches anything."
  (cons (list regexp-unmatchable) 'seq))

;; `cl-every' replacement to avoid bootstrapping problems.
(defun ry--every (pred list)
  "Whether PRED is true for every element of LIST."
  (while (and list (funcall pred (car list)))
    (setq list (cdr list)))
  (null list))

(defun ry--translate-or (body)
  "Translate an or-pattern of one of more ry items.
Return (REGEXP . PRECEDENCE)."
  ;; FIXME: Possible improvements:
  ;;
  ;; - Turn single characters to strings: (or ?a ?b) -> (or "a" "b"),
  ;;   so that they can be candidates for regexp-opt.
  ;;
  ;; - Translate compile-time strings (`eval' forms), again for regexp-opt.
  ;;
  ;; - Flatten sub-patterns first: (or (or A B) (or C D)) -> (or A B C D)
  ;;   in order to improve effectiveness of regexp-opt.
  ;;   This would also help composability.
  ;;
  ;; - Use associativity to run regexp-opt on contiguous subsets of arguments
  ;;   if not all of them are strings.  Example:
  ;;   (or (+ digit) "CHARLIE" "CHAN" (+ blank))
  ;;   -> (or (+ digit) (or "CHARLIE" "CHAN") (+ blank))
  ;;
  ;; - Fuse patterns into a single character alternative if they fit.
  ;;   regexp-opt will do that if all are strings, but we want to do that for:
  ;;     * symbols that expand to classes: space, alpha, ...
  ;;     * character alternatives: (any ...)
  ;;     * (syntax S), for some S (whitespace, word)
  ;;   so that (or "@" "%" digit (any "A-Z" space) (syntax word))
  ;;        -> (any "@" "%" digit "A-Z" space word)
  ;;        -> "[A-Z@%[:digit:][:space:][:word:]]"
  ;;
  ;; Problem: If a subpattern is carefully written to to be
  ;; optimisable by regexp-opt, how do we prevent the transforms
  ;; above from destroying that property?
  ;; Example: (or "a" (or "abc" "abd" "abe"))
  (cond
   ((null body)                    ; No items: a never-matching regexp.
    (ry--empty))
   ((null (cdr body))              ; Single item.
    (ry--translate (car body)))
   ((ry--every #'stringp body)     ; All strings.
    (cons (list (regexp-opt body nil t))
          t))
   (t
    (cons (append (car (ry--translate (car body)))
                  (mapcan (lambda (item)
                            (cons "\\|" (car (ry--translate item))))
                          (cdr body)))
          nil))))

(defun ry--string-to-intervals (str)
  "Decode STR as intervals: A-Z becomes (?A . ?Z), and the single
character X becomes (?X . ?X).  Return the intervals in a list."
  ;; We could just do string-to-multibyte on the string and work with
  ;; that instead of this `decode-char' workaround.
  (let ((decode-char
         (if (multibyte-string-p str)
             #'identity
           (lambda (c) (if (<= #x80 c #xff)
                           (+ c #x3fff00)
                         c))))
        (len (length str))
        (i 0)
        (intervals nil))
    (while (< i len)
      (cond ((and (< i (- len 2))
                  (= (aref str (1+ i)) ?-))
             ;; Range.
             (let ((start (funcall decode-char (aref str i)))
                   (end   (funcall decode-char (aref str (+ i 2)))))
               (cond ((and (<= start #x7f) (>= end #x3fff80))
                      ;; Ranges between ASCII and raw bytes are split to
                      ;; avoid having them absorb Unicode characters
                      ;; caught in-between.
                      (push (cons start #x7f) intervals)
                      (push (cons #x3fff80 end) intervals))
                     ((<= start end)
                      (push (cons start end) intervals))
                     (t
                      (error "Invalid ry `any' range: %s"
                             (substring str i 3))))
               (setq i (+ i 3))))
            (t
             ;; Single character.
             (let ((char (funcall decode-char (aref str i))))
               (push (cons char char) intervals))
             (setq i (+ i 1)))))
    intervals))

(defun ry--condense-intervals (intervals)
  "Merge adjacent and overlapping intervals by mutation, preserving the order.
INTERVALS is a list of (START . END) with START ≤ END, sorted by START."
  (let ((tail intervals)
        d)
    (while (setq d (cdr tail))
      (if (>= (cdar tail) (1- (caar d)))
          (progn
            (setcdr (car tail) (max (cdar tail) (cdar d)))
            (setcdr tail (cdr d)))
        (setq tail d)))
    intervals))

;; FIXME: Consider expanding definitions inside (any ...) and (not ...),
;; and perhaps allow (any ...) inside (any ...).
;; It would be benefit composability (build a character alternative by pieces)
;; and be handy for obtaining the complement of a defined set of
;; characters.  (See, for example, python.el:421, `not-simple-operator'.)
;; (Expansion in other non-rx positions is probably not a good idea:
;; syntax, category, backref, and the integer parameters of group-n,
;; =, >=, **, repeat)
;; Similar effect could be attained by ensuring that
;; (or (any X) (any Y)) -> (any X Y), and find a way to compose negative
;; sets.  `and' is taken, but we could add
;; (intersection (not (any X)) (not (any Y))) -> (not (any X Y)).

(defun ry--translate-any (negated body)
  "Translate an (any ...) construct.  Return (REGEXP . PRECEDENCE).
If NEGATED, negate the sense."
  (let ((classes nil)
        (strings nil)
        (conses nil))
    ;; Collect strings, conses and characters, and classes in separate bins.
    (dolist (arg body)
      (cond ((stringp arg)
             (push arg strings))
            ((and (consp arg)
                  (characterp (car arg))
                  (characterp (cdr arg))
                  (<= (car arg) (cdr arg)))
             ;; Copy the cons, in case we need to modify it.
             (push (cons (car arg) (cdr arg)) conses))
            ((characterp arg)
             (push (cons arg arg) conses))
            ((and (symbolp arg)
                  (let ((class (cdr (assq arg ry--char-classes))))
                    (and class (push class classes)))))
            (t (error "Invalid ry `any' argument: %s" arg))))
    (let ((items
           ;; Translate strings and conses into nonoverlapping intervals,
           ;; and add classes as symbols at the end.
           (append
            (ry--condense-intervals
             (sort (append conses
                           (mapcan #'ry--string-to-intervals strings))
                   #'car-less-than-car))
            (reverse classes))))

      ;; Move lone ] and range ]-x to the start.
      (let ((rbrac-l (assq ?\] items)))
        (when rbrac-l
          (setq items (cons rbrac-l (delq rbrac-l items)))))

      ;; Split x-] and move the lone ] to the start.
      (let ((rbrac-r (rassq ?\] items)))
        (when (and rbrac-r (not (eq (car rbrac-r) ?\])))
          (setcdr rbrac-r ?\\)
          (setq items (cons '(?\] . ?\]) items))))
      
      ;; Split ,-- (which would end up as ,- otherwise).
      (let ((dash-r (rassq ?- items)))
        (when (eq (car dash-r) ?,)
          (setcdr dash-r ?,)
          (setq items (nconc items '((?- . ?-))))))

      ;; Remove - (lone or at start of interval)
      (let ((dash-l (assq ?- items)))
        (when dash-l
          (if (eq (cdr dash-l) ?-)
              (setq items (delq dash-l items))   ; Remove lone -
            (setcar dash-l ?.))                  ; Reduce --x to .-x
          (setq items (nconc items '((?- . ?-))))))

      ;; Deal with leading ^ and range ^-x.
      (when (and (consp (car items))
                 (eq (caar items) ?^)
                 (cdr items))
        ;; Move ^ and ^-x to second place.
        (setq items (cons (cadr items)
                          (cons (car items) (cddr items)))))

      (cond
       ;; Empty set: if negated, any char, otherwise match-nothing.
       ((null items)
        (if negated
            (ry--translate-symbol 'anything)
          (ry--empty)))
       ;; Single non-negated character.
       ((and (null (cdr items))
             (consp (car items))
             (eq (caar items) (cdar items))
             (not negated))
        (cons (list (regexp-quote (char-to-string (caar items))))
              t))
       ;; At least one character or class, possibly negated.
       (t
        (cons
         (list
          (concat
           "["
           (and negated "^")
           (mapconcat (lambda (item)
                        (cond ((symbolp item)
                               (format "[:%s:]" item))
                              ((eq (car item) (cdr item))
                               (char-to-string (car item)))
                              ((eq (1+ (car item)) (cdr item))
                               (string (car item) (cdr item)))
                              (t
                               (string (car item) ?- (cdr item)))))
                      items nil)
           "]"))
         t))))))

(defun ry--translate-not (negated body)
  "Translate a (not ...) construct.  Return (REGEXP . PRECEDENCE).
If NEGATED, negate the sense (thus making it positive)."
  (unless (and body (null (cdr body)))
    (error "ry `not' form takes exactly one argument"))
  (let ((arg (car body)))
    (cond
     ((consp arg)
      (pcase (car arg)
        ((or 'any 'in 'char) (ry--translate-any      (not negated) (cdr arg)))
        ('syntax             (ry--translate-syntax   (not negated) (cdr arg)))
        ('category           (ry--translate-category (not negated) (cdr arg)))
        ('not                (ry--translate-not      (not negated) (cdr arg)))
        (_ (error "Illegal argument to ry `not': %S" arg))))
     ((eq arg 'word-boundary)
      (ry--translate-symbol
       (if negated 'word-boundary 'not-word-boundary)))
     (t
      (let ((class (cdr (assq arg ry--char-classes))))
        (if class
            (ry--translate-any (not negated) (list class))
          (error "Illegal argument to ry `not': %s" arg)))))))

(defun ry--atomic-regexp (item)
  "ITEM is (REGEXP . PRECEDENCE); return a regexp of precedence t."
  (if (eq (cdr item) t)
      (car item)
    (ry--bracket (car item))))

(defun ry--translate-counted-repetition (min-count max-count body)
  (let ((operand (ry--translate-seq body)))
    (if (car operand)
        (cons (append
               (ry--atomic-regexp operand)
               (list (concat "\\{"
                             (number-to-string min-count)
                             (cond ((null max-count) ",")
                                   ((< min-count max-count)
                                    (concat "," (number-to-string max-count))))
                             "\\}")))
              t)
      operand)))

(defun ry--check-repeat-arg (name min-args body)
  (unless (>= (length body) min-args)
    (error "ry `%s' requires at least %d argument%s"
           name min-args (if (= min-args 1) "" "s")))
  ;; There seems to be no reason to disallow zero counts.
  (unless (natnump (car body))
    (error "ry `%s' first argument must be nonnegative" name)))

(defun ry--translate-bounded-repetition (name body)
  (let ((min-count (car body))
        (max-count (cadr body))
        (items (cddr body)))
    (unless (and (natnump min-count)
                 (natnump max-count)
                 (<= min-count max-count))
      (error "ry `%s' range error" name))
    (ry--translate-counted-repetition min-count max-count items)))

(defun ry--translate-repeat (body)
  (ry--check-repeat-arg 'repeat 2 body)
  (if (= (length body) 2)
      (ry--translate-counted-repetition (car body) (car body) (cdr body))
    (ry--translate-bounded-repetition 'repeat body)))

(defun ry--translate-** (body)
  (ry--check-repeat-arg '** 2 body)
  (ry--translate-bounded-repetition '** body))

(defun ry--translate->= (body)
  (ry--check-repeat-arg '>= 1 body)
  (ry--translate-counted-repetition (car body) nil (cdr body)))

(defun ry--translate-= (body)
  (ry--check-repeat-arg '= 1 body)
  (ry--translate-counted-repetition (car body) (car body) (cdr body)))

(defvar ry--greedy t)

(defun ry--translate-rep (op-string greedy body)
  "Translate a repetition; OP-STRING is one of \"*\", \"+\" or \"?\".
GREEDY is a boolean.  Return (REGEXP . PRECEDENCE)."
  (let ((operand (ry--translate-seq body)))
    (if (car operand)
        (cons (append (ry--atomic-regexp operand)
                      (list (concat op-string (unless greedy "?"))))
              ;; The result has precedence seq to avoid (? (* "a")) -> "a*?"
              'seq)
      operand)))

(defun ry--control-greedy (greedy body)
  "Translate the sequence BODY with greediness GREEDY.
Return (REGEXP . PRECEDENCE)."
  (let ((ry--greedy greedy))
    (ry--translate-seq body)))

(defun ry--translate-group (body)
  "Translate the `group' form.  Return (REGEXP . PRECEDENCE)."
  (cons (ry--enclose "\\("
                     (car (ry--translate-seq body))
                     "\\)")
        t))

(defun ry--translate-group-n (body)
  "Translate the `group-n' form.  Return (REGEXP . PRECEDENCE)."
  (unless (and (integerp (car body)) (> (car body) 0))
    (error "ry `group-n' requires a positive number as first argument"))
  (cons (ry--enclose (concat "\\(?" (number-to-string (car body)) ":")
                     (car (ry--translate-seq (cdr body)))
                     "\\)")
        t))

(defun ry--translate-backref (body)
  "Translate the `backref' form.  Return (REGEXP . PRECEDENCE)."
  (unless (and (= (length body) 1) (integerp (car body)) (<= 1 (car body) 9))
    (error "ry `backref' requires an argument in the range 1..9"))
  (cons (list "\\" (number-to-string (car body))) t))

(defconst ry--syntax-codes
  '((whitespace         . ?-)           ; SPC also accepted
    (punctuation        . ?.)
    (word               . ?w)           ; W also accepted
    (symbol             . ?_)
    (open-parenthesis   . ?\()
    (close-parenthesis  . ?\))
    (expression-prefix  . ?\')
    (string-quote       . ?\")
    (paired-delimiter   . ?$)
    (escape             . ?\\)
    (character-quote    . ?/)
    (comment-start      . ?<)
    (comment-end        . ?>)
    (string-delimiter   . ?|)
    (comment-delimiter  . ?!)))

(defun ry--translate-syntax (negated body)
  "Translate the `syntax' form.  Return (REGEXP . PRECEDENCE)."
  (unless (and body (null (cdr body)))
    (error "ry `syntax' form takes exactly one argument"))
  (let* ((sym (car body))
         (syntax (cdr (assq sym ry--syntax-codes))))
    (unless syntax
      (cond
       ;; Syntax character directly (sregex compatibility)
       ((and (characterp sym) (rassq sym ry--syntax-codes))
        (setq syntax sym))
       ;; Syntax character as symbol (sregex compatibility)
       ((symbolp sym)
        (let ((name (symbol-name sym)))
          (when (= (length name) 1)
            (let ((char (string-to-char name)))
              (when (rassq char ry--syntax-codes)
                (setq syntax char)))))))
      (unless syntax
        (error "Unknown ry syntax name `%s'" sym)))
    (cons (list (string ?\\ (if negated ?S ?s) syntax))
          t)))

(defconst ry--categories
  '((space-for-indent           . ?\s)
    (base                       . ?.)
    (consonant                  . ?0)
    (base-vowel                 . ?1)
    (upper-diacritical-mark     . ?2)
    (lower-diacritical-mark     . ?3)
    (tone-mark                  . ?4)
    (symbol                     . ?5)
    (digit                      . ?6)
    (vowel-modifying-diacritical-mark . ?7)
    (vowel-sign                 . ?8)
    (semivowel-lower            . ?9)
    (not-at-end-of-line         . ?<)
    (not-at-beginning-of-line   . ?>)
    (alpha-numeric-two-byte     . ?A)
    (chinese-two-byte           . ?C)
    (chinse-two-byte            . ?C)   ; A typo in Emacs 21.1-24.3.
    (greek-two-byte             . ?G)
    (japanese-hiragana-two-byte . ?H)
    (indian-two-byte            . ?I)
    (japanese-katakana-two-byte . ?K)
    (strong-left-to-right       . ?L)
    (korean-hangul-two-byte     . ?N)
    (strong-right-to-left       . ?R)
    (cyrillic-two-byte          . ?Y)
    (combining-diacritic        . ?^)
    (ascii                      . ?a)
    (arabic                     . ?b)
    (chinese                    . ?c)
    (ethiopic                   . ?e)
    (greek                      . ?g)
    (korean                     . ?h)
    (indian                     . ?i)
    (japanese                   . ?j)
    (japanese-katakana          . ?k)
    (latin                      . ?l)
    (lao                        . ?o)
    (tibetan                    . ?q)
    (japanese-roman             . ?r)
    (thai                       . ?t)
    (vietnamese                 . ?v)
    (hebrew                     . ?w)
    (cyrillic                   . ?y)
    (can-break                  . ?|)))

(defun ry--translate-category (negated body)
  "Translate the `category' form.  Return (REGEXP . PRECEDENCE)."
  (unless (and body (null (cdr body)))
    (error "ry `category' form takes exactly one argument"))
  (let* ((arg (car body))
         (category
          (cond ((symbolp arg)
                 (let ((cat (assq arg ry--categories)))
                   (unless cat
                     (error "Unknown ry category `%s'" arg))
                   (cdr cat)))
                ((characterp arg) arg)
                (t (error "Invalid ry `category' argument `%s'" arg)))))
    (cons (list (string ?\\ (if negated ?C ?c) category))
          t)))

(defvar ry--delayed-evaluation nil
  "Whether to allow certain forms to be evaluated at runtime.")

(defun ry--translate-literal (body)
  "Translate the `literal' form.  Return (REGEXP . PRECEDENCE)."
  (unless (and body (null (cdr body)))
    (error "ry `literal' form takes exactly one argument"))
  (let ((arg (car body)))
    (cond ((stringp arg)
           (cons (list (regexp-quote arg)) (if (= (length arg) 1) t 'seq)))
          (ry--delayed-evaluation
           (cons (list (list 'regexp-quote arg)) 'seq))
          (t (error "ry `literal' form with non-string argument")))))

(defun ry--translate-eval (body)
  "Translate the `eval' form.  Return (REGEXP . PRECEDENCE)."
  (unless (and body (null (cdr body)))
    (error "ry `eval' form takes exactly one argument"))
  (ry--translate (eval (car body))))

(defvar ry--regexp-atomic-regexp nil)

(defun ry--translate-regexp (body)
  "Translate the `regexp' form.  Return (REGEXP . PRECEDENCE)."
  (unless (and body (null (cdr body)))
    (error "ry `regexp' form takes exactly one argument"))
  (let ((arg (car body)))
    (cond ((stringp arg)
           ;; Generate the regexp when needed, since ry isn't
           ;; necessarily present in the byte-compilation environment.
           (unless ry--regexp-atomic-regexp
             (setq ry--regexp-atomic-regexp
                   ;; Match atomic (precedence t) regexps: may give
                   ;; false negatives but no false positives, assuming
                   ;; the target string is syntactically correct.
                   (ry-to-string
                    '(seq
                      bos
                      (or (seq "["
                               (opt "^")
                               (opt "]")
                               (* (or (seq "[:" (+ (any "a-z")) ":]")
                                      (not (any "]"))))
                               "]")
                          anything
                          (seq "\\"
                               (or anything
                                   (seq (any "sScC_") anything)
                                   (seq "("
                                        (* (or (not (any "\\"))
                                               (seq "\\" (not (any ")")))))
                                        "\\)"))))
                      eos)
                    t)))
           (cons (list arg)
                 (if (string-match-p ry--regexp-atomic-regexp arg) t nil)))
          (ry--delayed-evaluation
           (cons (list arg) nil))
          (t (error "ry `regexp' form with non-string argument")))))

(defun ry--translate-compat-form (def form)
  "Translate a compatibility form from `rx-constituents'.
DEF is the definition tuple.  Return (REGEXP . PRECEDENCE)."
  (let* ((fn (nth 0 def))
         (min-args (nth 1 def))
         (max-args (nth 2 def))
         (predicate (nth 3 def))
         (nargs (1- (length form))))
    (when (< nargs min-args)
      (error "The `%s' form takes at least %d argument(s)"
             (car form) min-args))
    (when (and max-args (> nargs max-args))
      (error "The `%s' form takes at most %d argument(s)"
             (car form) max-args))
    (when (and predicate (not (ry--every predicate (cdr form))))
      (error "The `%s' form requires arguments satisfying `%s'"
             (car form) predicate))
    (let ((regexp (funcall fn form)))
      (unless (stringp regexp)
        (error "The `%s' form did not expand to a string" (car form)))
      (cons (list regexp) nil))))

(defun ry--substitute (bindings form)
  "Substitute BINDINGS in FORM."
  (cond ((symbolp form)
         (let ((binding (assq form bindings)))
           (if binding
               (cdr binding)
             form)))
        ((consp form)
         (if (listp (cdr form))
             ;; Proper list.  We substitute variables even in the head
             ;; position -- who knows, might be handy one day.
             (mapcar (lambda (x) (ry--substitute bindings x)) form)
           ;; Cons pair (presumably an interval).
           (cons (ry--substitute bindings (car form))
                 (ry--substitute bindings (cdr form)))))
        (t form)))

;; FIXME: Consider adding extensions in Lisp macro style, where
;; arguments are passed unevaluated to code that returns the ry form
;; to use.  Example:
;;
;;   (ry-let ((radix-digit (radix)
;;             :lisp (list 'any (cons ?0 (+ ?0 (eval radix) -1)))))
;;     (ry (radix-digit (+ 5 3))))
;; =>
;;   "[0-7]"
;;
;; While this would permit more powerful extensions, it's unclear just
;; how often they would be used in practice.  Let's wait until there is
;; demand for it.

;; FIXME: An alternative binding syntax would be
;;
;;   (NAME RXs...)
;; and
;;   ((NAME ARGS...) RXs...)
;;
;; which would have two minor advantages: multiple RXs with implicit
;; `seq' in the definition, and the arglist is no longer an optional
;; element in the middle of the list.  On the other hand, it's less
;; like traditional lisp arglist constructs (defun, defmacro).
;; Since it's a Scheme-like syntax, &rest parameters could be done using
;; dotted lists:
;;  (ry-let (((name arg1 arg2 . rest) ...definition...)) ...)

;; FIXME: We currently bind &rest parameters to (seq ...),
;; but it would probably be better to make them auto-splicing
;; so that they are not forced to be a sequence.  For example:
;; (ry-let ((colour (&rest choices) (or "black" choices)))
;;   (ry (colour "brown" "beige")))

(defun ry--expand-template (op values arglist template)
  "Return TEMPLATE with variables in ARGLIST replaced with VALUES."
  (let ((bindings nil)
        (value-tail values)
        (formals arglist))
    (while formals
      (pcase (car formals)
        ('&rest
         (unless (cdr formals)
           (error
            "Expanding ry def `%s': missing &rest parameter name" op))
         (push (cons (cadr formals) (cons 'seq value-tail)) bindings)
         (setq formals nil)
         (setq value-tail nil))
        (name
         (unless value-tail
           (error
            "Expanding ry def `%s': too few arguments (got %d, need %s%d)"
            op (length values)
            (if (memq '&rest arglist) "at least " "")
            (- (length arglist) (length (memq '&rest arglist)))))
         (push (cons name (car value-tail)) bindings)
         (setq value-tail (cdr value-tail))))
      (setq formals (cdr formals)))
    (when value-tail
      (error
       "Expanding ry def `%s': too many arguments (got %d, need %d)"
       op (length values) (length arglist)))
    (ry--substitute bindings template)))

(defun ry--translate-form (form)
  "Translate an ry form (list structure).  Return (REGEXP . PRECEDENCE)."
  (let ((body (cdr form)))
    (pcase (car form)
      ((or 'seq : 'and 'sequence) (ry--translate-seq body))
      ((or 'or '|)              (ry--translate-or body))
      ((or 'any 'in 'char)      (ry--translate-any nil body))
      ('not-char                (ry--translate-any t body))
      ('not                     (ry--translate-not nil body))

      ('repeat                  (ry--translate-repeat body))
      ('=                       (ry--translate-= body))
      ('>=                      (ry--translate->= body))
      ('**                      (ry--translate-** body))

      ((or 'zero-or-more '0+)           (ry--translate-rep "*" ry--greedy body))
      ((or 'one-or-more '1+)            (ry--translate-rep "+" ry--greedy body))
      ((or 'zero-or-one 'opt 'optional) (ry--translate-rep "?" ry--greedy body))

      ('*                       (ry--translate-rep "*" t body))
      ('+                       (ry--translate-rep "+" t body))
      ((or '\? ?\s)             (ry--translate-rep "?" t body))

      ('*?                      (ry--translate-rep "*" nil body))
      ('+?                      (ry--translate-rep "+" nil body))
      ((or '\?? ??)             (ry--translate-rep "?" nil body))

      ('minimal-match           (ry--control-greedy nil body))
      ('maximal-match           (ry--control-greedy t   body))

      ((or 'group 'submatch)     (ry--translate-group body))
      ((or 'group-n 'submatch-n) (ry--translate-group-n body))
      ('backref                  (ry--translate-backref body))

      ('syntax                  (ry--translate-syntax nil body))
      ('not-syntax              (ry--translate-syntax t body))
      ('category                (ry--translate-category nil body))

      ('literal                 (ry--translate-literal body))
      ('eval                    (ry--translate-eval body))
      ((or 'regexp 'regex)      (ry--translate-regexp body))

      (op
       (let ((definition (ry--lookup-def op)))
         (if definition
             (if (cddr definition)
                 (ry--translate
                  (ry--expand-template
                   op body (nth 1 definition) (nth 2 definition)))
               (error "Not an `ry' form definition: %s" op))

           ;; For compatibility with old rx.
           (let ((entry (assq op rx-constituents)))
             (if (progn
                   (while (and entry (not (consp (cdr entry))))
                     (setq entry
                           (if (symbolp (cdr entry))
                               ;; Alias for another entry.
                               (assq (cdr entry) rx-constituents)
                             ;; Wrong type, try further down the list.
                             (assq (car entry)
                                   (cdr (memq entry rx-constituents))))))
                   entry)
                 (ry--translate-compat-form (cdr entry) form)
               (error "Unknown ry form `%s'" op)))))))))


(defun ry--translate (item)
  "Translate the ry-expression ITEM.  Return (REGEXP . PRECEDENCE)."
  (cond
   ((stringp item)
    (if (= (length item) 0)
        (cons nil 'seq)
      (cons (list (regexp-quote item)) (if (= (length item) 1) t 'seq))))
   ((characterp item)
    (cons (list (regexp-quote (char-to-string item))) t))
   ((symbolp item)
    (ry--translate-symbol item))
   ((consp item)
    (ry--translate-form item))
   (t (error "Bad ry expression: %S" item))))
   

;;;###autoload
(defun ry-to-string (form &optional no-group)
  "Translate FORM from `ry' sexp syntax into a string regexp.
The arguments to `literal' and `regexp' forms inside FORM must be
constant strings.
If NO-GROUP is non-nil, don't bracket the result in a non-capturing
group."
  (let* ((item (ry--translate form))
         (exprs (if no-group
                    (car item)
                  (ry--atomic-regexp item))))
    (apply #'concat exprs)))

(defun ry--to-expr (form)
  "Translate the ry-expression FORM to a Lisp expression yielding a regexp."
  (let* ((ry--delayed-evaluation t)
         (elems (car (ry--translate form)))
         (args nil))
    ;; Merge adjacent strings.
    (while elems
      (let ((strings nil))
        (while (and elems (stringp (car elems)))
          (push (car elems) strings)
          (setq elems (cdr elems)))
        (let ((s (apply #'concat (nreverse strings))))
          (unless (zerop (length s))
            (push s args))))
      (when elems
        (push (car elems) args)
        (setq elems (cdr elems))))
    (cond ((null args) "")                             ; 0 args
          ((cdr args) (cons 'concat (nreverse args)))  ; ≥2 args
          (t (car args)))))                            ; 1 arg


;;;###autoload
(defmacro ry (&rest regexps)
  "Translate regular expressions REGEXPS in sexp form to a regexp string.
Each argument is one of the forms below; RX is a subform, and RX... stands
for one or more RXs.  For details, see Info node `(elisp) Rx Notation'.
See `rx-to-string' for the corresponding function.

STRING         Match a literal string.
CHAR           Match a literal character.

(seq RX...)    Match the RXs in sequence.  Alias: :, sequence, and.
(or RX...)     Match one of the RXs.  Alias: |.

(zero-or-more RX...) Match RXs zero or more times.  Alias: 0+.
(one-or-more RX...)  Match RXs one or more times.  Alias: 1+.
(zero-or-one RX...)  Match RXs or the empty string.  Alias: opt, optional.
(* RX...)       Match RXs zero or more times; greedy.
(+ RX...)       Match RXs one or more times; greedy.
(? RX...)       Match RXs or the empty string; greedy.
(*? RX...)      Match RXs zero or more times; non-greedy.
(+? RX...)      Match RXs one or more times; non-greedy.
(?? RX...)      Match RXs or the empty string; non-greedy.
(= N RX...)     Match RXs exactly N times.
(>= N RX...)    Match RXs N or more times.
(** N M RX...)  Match RXs N to M times.  Alias: repeat.
(minimal-match RX)  Match RX, with zero-or-more, one-or-more, zero-or-one
                and aliases using non-greedy matching.
(maximal-match RX)  Match RX, with zero-or-more, one-or-more, zero-or-one
                and aliases using greedy matching, which is the default.

(any SET...)    Match a character from one of the SETs.  Each SET is a
                character, a string, a range as string \"A-Z\" or cons
                (?A . ?Z), or a character class (see below).  Alias: in, char.
(not CHARSPEC)  Match one character not matched by CHARSPEC.  CHARSPEC
                can be (any ...), (syntax ...), (category ...),
                or a character class.
not-newline     Match any character except a newline.  Alias: nonl.
anything        Match any character.

CHARCLASS       Match a character from a character class.  One of:
 alpha, alphabetic, letter   Alphabetic characters (defined by Unicode).
 alnum, alphanumeric         Alphabetic or decimal digit chars (Unicode).
 digit numeric, num          0-9.
 xdigit, hex-digit, hex      0-9, A-F, a-f.
 cntrl, control              ASCII codes 0-31.
 blank                       Horizontal whitespace (Unicode).
 space, whitespace, white    Chars with whitespace syntax.
 lower, lower-case           Lower-case chars, from current case table.
 upper, upper-case           Upper-case chars, from current case table.
 graph, graphic              Graphic characters (Unicode).
 print, printing             Whitespace or graphic (Unicode).
 punct, punctuation          Not control, space, letter or digit (ASCII);
                              not word syntax (non-ASCII).
 word, wordchar              Characters with word syntax.
 ascii                       ASCII characters (codes 0-127).
 nonascii                    Non-ASCII characters (but not raw bytes).

(syntax SYNTAX)  Match a character with syntax SYNTAX, being one of:
  whitespace, punctuation, word, symbol, open-parenthesis,
  close-parenthesis, expression-prefix, string-quote,
  paired-delimiter, escape, character-quote, comment-start,
  comment-end, string-delimiter, comment-delimiter

(category CAT)   Match a character in category CAT, being one of:
  space-for-indent, base, consonant, base-vowel,
  upper-diacritical-mark, lower-diacritical-mark, tone-mark, symbol,
  digit, vowel-modifying-diacritical-mark, vowel-sign,
  semivowel-lower, not-at-end-of-line, not-at-beginning-of-line,
  alpha-numeric-two-byte, chinese-two-byte, greek-two-byte,
  japanese-hiragana-two-byte, indian-two-byte,
  japanese-katakana-two-byte, strong-left-to-right,
  korean-hangul-two-byte, strong-right-to-left, cyrillic-two-byte,
  combining-diacritic, ascii, arabic, chinese, ethiopic, greek,
  korean, indian, japanese, japanese-katakana, latin, lao,
  tibetan, japanese-roman, thai, vietnamese, hebrew, cyrillic,
  can-break

Zero-width assertions: these all match the empty string in specific places.
 line-start         At the beginning of a line.  Alias: bol.
 line-end           At the end of a line.  Alias: eol.
 string-start       At the start of the string or buffer.
                     Alias: buffer-start, bos, bot.
 string-end         At the end of the string or buffer.
                     Alias: buffer-end, eos, eot.
 point              At point.
 word-start         At the beginning of a word.
 word-end           At the end of a word.
 word-boundary      At the beginning or end of a word.
 not-word-boundary  Not at the beginning or end of a word.
 symbol-start       At the beginning of a symbol.
 symbol-end         At the end of a symbol.

(group RX...)  Match RXs and define a capture group.  Alias: submatch.
(group-n N RX...) Match RXs and define capture group N.  Alias: submatch-n.
(backref N)    Match the text that capture group N matched.

(literal EXPR) Match the literal string from evaluating EXPR at run time.
(regexp EXPR)  Match the string regexp from evaluating EXPR at run time.
(eval EXPR)    Match the rx sexp from evaluating EXPR at compile time.

Additional constructs can be defined using `ry-define' and `ry-let',
which see.

\(fn REGEXPS...)"
  (ry--to-expr (cons 'seq regexps)))

(defun ry--make-binding (bindspec)
  "Make a definitions entry out of BINDSPEC.
BINDSPEC is on the form (NAME [ARGLIST] DEFINITION)."
  (unless (consp bindspec)
    (error "Bad `ry-let' binding: %S" bindspec))
  (let ((name (car bindspec))
        (tail (cdr bindspec)))
    (unless (symbolp name)
      (error "Bad `ry' definition name: %S" name))
    (cons name
          (pcase tail
            (`(,def)
             (list def))
            (`(,args ,def)
             (unless (and (listp args) (ry--every #'symbolp args))
               (error "Bad argument list for `ry' definition %s: %S" name args))
             (list args def))
            (_ (error "Bad `ry' definition of %s: %S" name tail))))))

(defun ry--extend-local-defs (bindspecs)
  (append (mapcar #'ry--make-binding bindspecs)
          ry--local-definitions))

;;;###autoload
(defmacro ry-let-eval (bindings &rest body)
  "Evaluate BODY with local BINDINGS for `ry-to-string'.
BINDINGS, after evaluation, is a list of definitions each on the form
(NAME [(ARGS...)] DEFINITION), in effect for calls to `ry-to-string'
in BODY.

For bindings without an ARGS list, NAME is defined as an alias
for DEFINITION.  Where ARGS is supplied, NAME is defined as an
`ry' form with ARGS as argument list.  The parameters are bound
from the values in the (NAME ...) form and are substituted in
DEFINITIONS.  ARGS can contain `&rest' parameters, which are
automatically surrounded by `(seq ...)' upon substitution.

Any previous definitions with the same names are shadowed during
the expansion of BODY only.
For extensions when using the `ry' macro, use `ry-let'.
To make global ry extensions, use `ry-define'.

\(fn BINDINGS BODY...)"
  (declare (indent 1) (debug (form body)))
  `(let ((ry--local-definitions (ry--extend-local-defs ,bindings)))
     ,@body))

;;;###autoload
(defmacro ry-let (bindings &rest body)
  "Evaluate BODY with local BINDINGS for `ry'.
BINDINGS is an unevaluated list of bindings each on the form
(NAME [(ARGS...)] DEFINITION).
They are bound lexically and are available in `ry' expressions in
BODY only.

For bindings without an ARGS list, NAME is defined as an alias
for DEFINITION.  Where ARGS is supplied, NAME is defined as an
`ry' form with ARGS as argument list.  The parameters are bound
from the values in the (NAME ...) form and are substituted in
DEFINITIONS.  ARGS can contain `&rest' parameters, which are
automatically surrounded by `(seq ...)' upon substitution.

Any previous definitions with the same names are shadowed during
the expansion of BODY only.
For local extensions to `ry-to-string', use `ry-let-eval'.
To make global ry extensions, use `ry-define'.

\(fn BINDINGS BODY...)"
  (declare (indent 1) (debug (sexp body)))
  (ry-let-eval bindings
    (macroexpand-all (cons 'progn body))))

;;;###autoload
(defmacro ry-define (name &rest definition)
  "Define NAME as a global `ry' definition.
If the ARGS list is omitted, define NAME as an alias for DEFINITION
in `ry' forms.

If the ARGS list is supplied, define NAME as an `ry' form with
ARGS as argument list.  The parameters are bound from the values
in the (NAME ...) form and are substituted in DEFINITION.
ARGS can contain `&rest' parameters, which are automatically
surrounded by `(seq ...)' upon substitution.

Any previous global definition of NAME is overwritten with the new one.
To make local ry extensions, use `ry-let' for `ry',
`ry-let-eval' for `ry-to-string'.

\(fn NAME [(ARGS...)] DEFINITION)"
  (declare (indent 1))
  (setq ry--global-definitions
        (cons (ry--make-binding (cons name definition))
              (delq (assq name ry--global-definitions)
                    ry--global-definitions)))
  (list 'quote name))

(defmacro ry--save-definitions (&rest body)
  "Expand BODY and restore ry definitions afterwards.  For testing only."
  (let ((ry--global-definitions ry--global-definitions)
        (ry--local-definitions ry--local-definitions))
    (macroexpand-all `(progn ,@body))))

;; During `ry--pcase-transform', list of defined variables in right-to-left
;; order.
(defvar ry--pcase-vars)

;; FIXME: The rewriting strategy for pcase works so-so with extensions;
;; definitions cannot expand to `let' or named `backref'.  If this ever
;; becomes a problem, we can handle those forms in the ordinary parser,
;; using a dynamic variable for activating the augmented forms.

(defun ry--pcase-transform (ry)
  "Transform RY, an ry-expression augmented with `let' and named `backref',
into a plain ry-expression, collecting names into `ry--pcase-vars'."
  (pcase ry
    (`(let ,name . ,body)
     (let* ((index (length (memq name ry--pcase-vars)))
            (i (if (zerop index)
                   (length (push name ry--pcase-vars))
                 index)))
       `(group-n ,i ,(ry--pcase-transform (cons 'seq body)))))
    ((and `(backref ,ref)
          (guard (symbolp ref)))
     (let ((index (length (memq ref ry--pcase-vars))))
       (when (zerop index)
         (error "ry `backref' variable must be one of: %s"
                (mapconcat #'symbol-name ry--pcase-vars " ")))
       `(backref ,index)))
    ((and `(,head . ,rest)
          (guard (and (symbolp head)
                      (not (memq head '(literal regexp regex eval))))))
     (cons head (mapcar #'ry--pcase-transform rest)))
    (_ ry)))

(pcase-defmacro ry (&rest regexps)
  "A pattern that matches strings against `ry' REGEXPS in sexp form.
REGEXPS are interpreted as in `ry'.  The pattern matches any
string that is a match for REGEXPS, as if by `string-match'.

In addition to the usual `ry' syntax, REGEXPS can contain the
following constructs:

  (let REF RX...)  binds the symbol REF to a submatch that matches
                   the regular expressions RX.  REF is bound in
                   CODE to the string of the submatch or nil, but
                   can also be used in `backref'.
  (backref REF)    matches whatever the submatch REF matched.
                   REF can be a number, as usual, or a name
                   introduced by a previous (let REF ...)
                   construct."
  (let* ((ry--pcase-vars nil)
         (regexp (ry--to-expr (ry--pcase-transform (cons 'seq regexps)))))
    `(and (pred (string-match ,regexp))
          ,@(let ((i 0))
              (mapcar (lambda (name)
                        (setq i (1+ i))
                        `(app (match-string ,i) ,name))
                      (reverse ry--pcase-vars))))))

(provide 'ry)

;; ry.el ends here
